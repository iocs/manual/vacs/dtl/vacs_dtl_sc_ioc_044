#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tcp350
#
require vac_ctrl_tcp350,1.5.1


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tcp350_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: DTL-040:Vac-VEPT-01100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = DTL-040:Vac-VEPT-01100, IPADDR = moxa-vac-dtl-2.tn.esss.lu.se, PORT = 4009")

#
# Device: DTL-040:Vac-VPT-01100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = DTL-040:Vac-VPT-01100, CONTROLLERNAME = DTL-040:Vac-VEPT-01100")
