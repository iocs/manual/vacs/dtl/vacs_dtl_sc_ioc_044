# IOC for DTL-040 vacuum turbopumps

## Used modules

*   [vac_ctrl_tcp350](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tcp350)


## Controlled devices

*   DTL-040:Vac-VEPT-01100
    *   DTL-040:Vac-VPT-01100
